<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use App\Models\Registros;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // CONSUMO DE DATOS WEB SERVISE METODO GET 
        $consulta = Http::get('https://jsonplaceholder.typicode.com/todos/1');
        $datos = json_decode($consulta,true);
        // CONSUTA DE  DATOS DESDE DATABASE
        $registros =  Registros::all();
        return view('home',compact('datos','registros'));
    }
   
}
