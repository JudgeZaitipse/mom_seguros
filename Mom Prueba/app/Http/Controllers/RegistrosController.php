<?php

namespace App\Http\Controllers;

use App\Models\Registros;
use Illuminate\Http\Request;

class RegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Registros::all();
        if ($data->isEmpty()) {
            $resul = array(
                'status' => 'Registros No encontrado intente nuevamente'
            );
        }else{
           $resul = array(
                'status' => $data
            );
        }
        return response()->json($resul);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if (isset($request)) {
            $registro =  new Registros;
            $registro->userId = $request->userId;
            $registro->title = $request->title;
            $registro->completed = $request->completed;
            $registro->save();
            $data = array(
                'status'=> '200',
                'message' => 'Registros Creado Correctamente'
            );
        }else{
            $data = array(
                'status' => '404',
                'response' => 'Registro No encontrado intente nuevamente'
            );
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if (isset($request->id)) {
            $registro = Registros::find($request->id);
            if (isset($registro)) {
                $data = array(
                    'status' => '200',
                    'response' => $registro
                );
            }else{
                $data = array(
                    'status' => '404',
                    'response' => 'Registro No encontrado intente nuevamente'
                );
            }
        }else{
            $data = array(
                'status' => '404',
                'response' => 'Registro No encontrado intente nuevamente'
            );
        }
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (isset($request->id)) {
            $registro = Registros::find($request->id);
            if (isset($registro)) {
                $registro->userId = $request->userId;
                $registro->title = $request->title;
                $registro->completed = $request->completed;
                $registro->update();
                $data = array(
                    'status'=> '200',
                    'message' => 'Registros Actulizados Correctamente'
                );
            }else{
                $data = array(
                    'status' => '404',
                    'response' => 'Registro No encontrado intente nuevamente'
                );
            }
        }else{
            $data = array(
                'status' => '404',
                'response' => 'Registro No encontrado intente nuevamente'
            );
        }
        return response()->json($data);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Registros  $registros
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         if (isset($request->id)) {
            $registro = Registros::find($request->id);
            if (isset($registro)) {
                $registro->delete();
                $data = array(
                    'status'=> '200',
                    'message' => 'Registro Eliminado Correctamente'
                );
            }else{
                $data = array(
                    'status' => '404',
                    'response' => 'Registro No encontrado intente nuevamente'
                );
            }
        }else{
            $data = array(
                'status' => '404',
                'response' => 'Registro No encontrado intente nuevamente'
            );
        }
        return response()->json($data);
    }
}
