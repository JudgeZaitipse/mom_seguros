<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registros extends Model
{
    protected $table = 'registros';
    protected $fillable = ['userId','title','completed'];
    protected $guarded =['id'];  
}
