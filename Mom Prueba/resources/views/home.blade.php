@extends('layouts.app')

@section('content')
<div class="content mt-5">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">CONSUMO DE WEB SERVICE </h4>
            <p class="card-category">Lista de datos</p>
          </div>
          <div class="card-body">
          <div class="material-datatables">
           <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" >
                  <thead>
                      <tr>
                          <th>Nº Regitro:</th>
                          <th>Id Usuario:</th>
                          <th>Titulo:</th>
                          <th>Terminado:</th>
                      </tr>
                  </thead>
                  <tbody>
                    <tr>
                     <td>{{ $datos['id'] }}</td>
                     <td>{{ $datos['userId'] }}</td>
                     <td>{{ $datos['title'] }}</td>
                     <td>{{ $datos['completed'] == true ? 'SI' : 'NO' }}</td>
                    </tr>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- dabase  --}}
<div class="content mt-5">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">CONSUMO DE WEB SERVICE </h4>
            <p class="card-category">Lista de datos</p>
          </div>
          <div class="card-body">
          <div class="material-datatables">
           <table id="datatables-table" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" >
                  <thead>
                      <tr>
                          <th>Nº Regitro:</th>
                          <th>Id Usuario:</th>
                          <th>Titulo:</th>
                          <th>Terminado:</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($registros as $value)
                      <tr>
                         <td>{{ $value->id }}</td>
                         <td>{{ $value->userId }}</td>
                         <td>{{ $value->title }}</td>
                         <td>{{ $value->completed == 'true' ? 'SI' : 'NO' }}</td>
                      </tr>
                    @endforeach   
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
      // web servise DataTable
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todo"]
        ],
        responsive: true,
        language: {
          "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
        }
      });
      // databse DataTable
      $('#datatables-table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "Todo"]
        ],
        responsive: true,
        language: {
          "sProcessing":     "Procesando...",
              "sLengthMenu":     "Mostrar _MENU_ registros",
              "sZeroRecords":    "No se encontraron resultados",
              "sEmptyTable":     "Ningún dato disponible en esta tabla",
              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
              "sInfoPostFix":    "",
              "sSearch":         "Buscar:",
              "sUrl":            "",
              "sInfoThousands":  ",",
              "sLoadingRecords": "Cargando...",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
        }
      }); 

    });
  </script>
@endsection
