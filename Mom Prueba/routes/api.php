<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('consulta','RegistrosController');
Route::get('/register/show/{id}','RegistrosController@show');
Route::post('/register/create','RegistrosController@store');
Route::put('/register/update/{id}','RegistrosController@update');
Route::delete('/register/delete/{id}','RegistrosController@destroy');
