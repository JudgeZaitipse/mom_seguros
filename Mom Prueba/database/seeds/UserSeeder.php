<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Mon' ,
            'email' => 'mom@gmail.com',
            'password' => Hash::make('mom2021'),
        ]);
    }
}
