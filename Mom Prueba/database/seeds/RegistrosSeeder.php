<?php

use Illuminate\Database\Seeder;
use App\Models\Registros;

class RegistrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datosjson = file_get_contents("public/js/data.json");
    	$datos = json_decode($datosjson, true);
    	foreach ($datos as $value) {
    		$registros = new Registros;
    		$registros->userId = $value['userId'];
            $registros->title = $value['title'];
            $registros->completed = $value['completed'];
    		$registros->save();
    	}
    }
}
